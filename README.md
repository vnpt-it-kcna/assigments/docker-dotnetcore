# Docker build and run
docker build -t web-dotnetcore .
docker run -d --name web-dotnetcore -p 80:80 web-dotnetcore
- stop and remove
docker stop web-dotnetcore
docker rm web-dotnetcore

# Docker compose run
- up
docker-compose up -d
- down
docker-compose down